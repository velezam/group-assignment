﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Cloud_Runner
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LeaderBoard : Page
    {
        public Dictionary<string, double> scores = new Dictionary<string, double>();

        public LeaderBoard()
        {
            this.InitializeComponent();

            scores.Add("Shae", 689.5);
            scores.Add("Judy", 279.9);
            scores.Add("Beth", 172.4);
            scores.Add("Fred", 471.5);
            scores.Add("Kenny", 702.2);
            scores.Add("Blake", 743.2);
            scores.Add("Rico", 550.8);
            scores.Add("Danny", 362.4);
            scores.Add("Jemma", 182.7);
            scores.Add("Josh", 345.1);

            foreach (KeyValuePair<string, double> element in scores.OrderByDescending(key => key.Value))
            {
                LeaderBoardOutput.Text += $"{element.Key} : {element.Value} M" + Environment.NewLine;
            }    
        }

        private void BBackToMain_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(TitlePage));
        }
    }
}
