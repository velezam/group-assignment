﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud_Runner
{
    class Player
    {

        private int _distanceRun;

        private Player()
        {
            X = this.X;
            Y = this.Y;
            Height = this.Height;
            Width = this.Width;
            Speed = Speed;
            MaxSpeed = this.MaxSpeed;
        }

        private void Move()
        {
            Y += Speed;
        }

        //getters and setters
        public float X { get; set; }

        public float Y { get; set; }

        public float Height { get; set; }

        public float Width { get; set; }

        public float Speed { get; set; }

        public float MaxSpeed { get; set; }
    }
}
