﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Cryptography.Certificates;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Windows.Input;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Cloud_Runner
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
        enum Position
        {
            Up,
            Down
        }

        private Random _random = new Random();

        //create list of zappers which
        private List<Zapper> zappers = new List<Zapper>(5);
        private Bird bird = new Bird();

        //private Player player = new Player();
        private int _canvasWidth, _canvasHeight;

        private int _interval = 3;

        private DispatcherTimer _timer = new DispatcherTimer();

        private bool _isAccelerating, _resetSpeed;

        private int _speed = 5;

        private int _score;

        private Position position = Position.Down;

        public GamePage()
        {
            this.InitializeComponent();
        }

        private void GetSizes()
        {
            _canvasHeight = (int)MainCanvas.ActualHeight;
            _canvasWidth = (int)MainCanvas.ActualWidth;

            foreach (Zapper zapper in zappers)
            {
                zapper.X = _canvasWidth;
                zapper.Y = _random.Next(0, _canvasHeight / 20 * 19);
                zapper.X2 = zapper.X + 50;
                zapper.Y2 = zapper.Y - 50;
                zapper.Speed = -3;
            }
        }

        private void OnPageLoad(object sender, RoutedEventArgs e)
        {
            GetSizes();
            SetupTimer();
            SetupGame();
            DrawLine();
            Canvas.SetTop(Player, 500);
            Canvas.SetLeft(Player, 20);

        }

        private void OnKeyDown(object sender, KeyRoutedEventArgs e)
        {
            switch (e.Key)
            {
                case VirtualKey.Up:
                    MoveSpriteUp(Player);
                    break;
                case VirtualKey.Down:
                    MoveSpriteDown(Player);
                    break;
            }
        }

        //private int _stepSize = 5;

        //private void MoveSpriteUp(UIElement element)
        //{
        //    int y = (int)Canvas.GetTop(element) - _stepSize;
        //    if (y >= 0)
        //        Canvas.SetTop(element, y);
        //}

        //private void OnPlayerKeyDown(object sender, KeyRoutedEventArgs e)
        //{
        //    switch (e.Key)
        //    {
        //        case Windows.System.VirtualKey.Up:
        //            MoveSpriteUp(BPlayer);
        //            break;
        //    }
        //}

        private void MoveSpriteUp(UIElement element)
        {
            int y = (int)Canvas.GetTop(Player) - _speed;
            Canvas.SetTop(Player, y);
        }
        private void MoveSpriteDown(UIElement element)
        {
            if (Canvas.GetTop(Player) <= 500)
            {
                int y = (int)Canvas.GetTop(Player) + _speed;
                Canvas.SetTop(Player, y);
            }
        }

        private void SetupTimer()
        {
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            //_timer.Tick += OnTimerTick;
            _timer.Start();
        }

        private void OnTimerTick(object sender, object e)
        {
            foreach (Zapper zapper in zappers)
            {
                zapper.X -= 5;
                zapper.X2 -= 5;
            }
            DrawLine();
        }

        public void DrawLine()
        {
            Line newLine = new Line();
            newLine.X1 = 500;
            newLine.X2 = 600;
            newLine.Y1 = 500;
            newLine.Y2 = 600;

            foreach (Zapper zapper in zappers)
            {
                newLine = new Line();
                newLine.X1 = zapper.X;
                newLine.X2 = zapper.X2;
                newLine.Y1 = zapper.Y;
                newLine.Y2 = zapper.Y2;
            }
        }

        //GAME OVER
        private void DoGameOver()
        {
            Frame.Navigate(typeof(EndScreen), _score);
        }

        //SETUP
        private void SetupGame()
        {

            //Player movement variables
            _isAccelerating = false;
            _resetSpeed = false;

            //Create zappers
            for (int i = 0; i < zappers.Count; i++)
            {
                zappers[i] = new Zapper();

                if (i > 0)
                {
                    int secondPoint = _random.Next(4);
                    switch (secondPoint)
                    {
                        case 0:
                            zappers[i].X = zappers[i - 1].X + 75 + _canvasWidth / 5;
                            zappers[i].X2 = zappers[i].X;
                            zappers[i].Y2 = zappers[i].Y - 75;
                            break;
                        case 1:
                            zappers[i].X = zappers[i - 1].X + 75 + _canvasWidth / 5;
                            zappers[i].X2 = zappers[i].X + 50;
                            zappers[i].Y2 = zappers[i].Y - 50;
                            break;
                        case 2:
                            zappers[i].X = zappers[i - 1].X + 75 + _canvasWidth / 5;
                            zappers[i].X2 = zappers[i].X + 75;
                            zappers[i].Y2 = zappers[i].Y;
                            break;
                        case 3:
                            zappers[i].X = zappers[i - 1].X + 75 + _canvasWidth / 5;
                            zappers[i].X2 = zappers[i].X + 50;
                            zappers[i].Y2 = zappers[i].Y + 50;
                            break;
                    }
                }

                if (zappers[i].Y2 < 0)
                {
                    zappers[i].Y += 0 - zappers[i].Y2;
                    zappers[i].Y2 = 0;
                }
                else if (zappers[i].Y2 > _canvasHeight / 20 * 19 - 10)
                {
                    zappers[i].Y -= zappers[i].Y2 - _canvasHeight / 20 * 19 - 10;
                    zappers[i].Y2 = (_canvasHeight / 20 * 19 - 10);
                }
            }
        }
    }
}

