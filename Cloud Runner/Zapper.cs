﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Editing;
using Windows.UI.Xaml.Shapes;

namespace Cloud_Runner
{
    class Zapper
    {
        public Zapper()
        {
            X = this.X;
            Y = this.Y;
            X2 = this.X2;
            Y2 = this.Y2;
            Speed = this.Speed;
        }

        private void Move()
        {
            X += Speed;
            X2 += Speed;
        }

        //getters and setters
        public float X { get; set; }

        public float Y { get; set; }

        public float X2 { get; set; }

        public float Y2 { get; set; }

        public float Speed { get; set; }
    }
}
