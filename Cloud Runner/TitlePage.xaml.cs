﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Authentication.OnlineId;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Cloud_Runner
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TitlePage : Page
    {
        public TitlePage()
        {
            this.InitializeComponent();
        }

        private void BNavigateRequested(object sender, RoutedEventArgs e)
        {
            if (sender == BPlay)
            {
                Frame.Navigate(typeof(GamePage));
            }
            
            else if (sender == BLeaderboard)
            {
                Frame.Navigate(typeof(LeaderBoard));
            }

            else if (sender == BHowToPlay)
            {
                Frame.Navigate(typeof(How_To_Play));
            }
        }
    }
}
